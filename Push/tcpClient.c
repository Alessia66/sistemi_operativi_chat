#include <sys/socket.h>       
#include <sys/types.h>        
#include <arpa/inet.h>        
#include <unistd.h>           
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>   
#include "helper.h"         
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <signal.h>


#define MAX_LINE (1000)
#define LEGGI "LEGGI"
#define MURDERED "KILLED"
#define END "ENDED\n"
#define BACK "INDIETRO"
#define LOGIN "LOGIN"
#define REG "REGISTER"

#define REMOTE_SERVER_PORT 7007

int sockTCP;  

void handler(){ //Quando facciamo SIGINT e vogliamo che il thread lato server che ci gestisce muoia con noi 
	yellow();
	printf("\nAdios\n");
	reset();
	Writeline(sockTCP, MURDERED ,6);	
	exit(EXIT_SUCCESS);
}
void handler1(){ //Quando riceviamo SIGPIPE e di conseguenza ci killiamo
	yellow();
	printf("Il server si è spento\n");
	reset();
	exit(EXIT_SUCCESS);
}
ssize_t res = 0; // Quando va in errore la socket e read/write resituiscono un valore <=0 

int Tok(int argc, char *argv[], char **address, char **nPort) {

    int n = 1;

    while ( n < argc )
	{
		if ( !strncmp(argv[n], "-a", 2) || !strncmp(argv[n], "-A", 2) )
		{
		    *address = argv[++n];
		}
		else 
			if ( !strncmp(argv[n], "-p", 2) || !strncmp(argv[n], "-P", 2) )
			{
			    *nPort = argv[++n];
			}
			else
				if ( !strncmp(argv[n], "-h", 2) || !strncmp(argv[n], "-H", 2) )
				{
		    		printf("Sintassi Corretta:\n");
			    	printf("client -a (indirizzo remoto) -p (porta remota) [-h].\n");
			    	exit(EXIT_SUCCESS);
				}
		++n;
    }
	if (argc==1)
	{
	    printf("Sintassi Corretta:\n");
		printf("client -a (indirizzo remoto) -p (porta remota) [-h].\n");
	    exit(EXIT_SUCCESS);
	}
    return 0;
}

int main(int argc, char *argv[]) {     
	          
    short int port;                  
    struct    sockaddr_in servaddr;  
    char      buffer[MAX_LINE] = "";    
    char     *address;             
    char     *nPort;                
    char     *endptr;    
    char sonoio[MAX_LINE]={0};  
    struct hostent *h;    
    int sd, rc, ret, i;
	struct sockaddr_in cliAddr, remoteServAddr;      
	
	signal(SIGINT,handler);
	signal(SIGQUIT,handler);
	signal(SIGPIPE,handler1);
	
    Tok(argc, argv, &address, &nPort);

    port = strtol(nPort, &endptr, 0);
    if ( *endptr )
	{
		printf("Client: Porta non riconosciuta.\n");
		exit(EXIT_FAILURE);
    }
	

    if ( (sockTCP = socket(AF_INET, SOCK_STREAM, 0)) < 0 )
	{
		fprintf(stderr, "Client: Errore durante la creazione della socket.\n");
		exit(EXIT_FAILURE);
    }

	memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_port        = htons(port);

    if ( inet_aton(address, &servaddr.sin_addr) <= 0 )
	{
		printf("Client: Indirizzo IP non valido.\n");
			printf("Fallita.\n");
  			exit(EXIT_FAILURE);
		}
   
    
    if ( connect(sockTCP, (struct sockaddr *) &servaddr, sizeof(servaddr) ) < 0 )
	{
		printf("Client: Errore durante la connect.\n");
		exit(EXIT_FAILURE);
    }
    
    while(1){
		fucsia();
		memset(buffer,0,sizeof(char)*(strlen(buffer)+1));
		printf("Scrivi REGISTER per registrarti o LOGIN per loggarti ");
		fgets(buffer,MAX_LINE,stdin);
		if(strncmp(buffer,LOGIN,5)==0){
			res = Writeline(sockTCP, buffer, strlen(buffer));
			if(res<=0) handler1();
			break;
		}
		else if(strncmp(buffer,REG,8)==0){
			res= Writeline(sockTCP, buffer,strlen(buffer));	
			if(res<=0) handler1();
			break;
		}
	}
	if(strncmp(buffer,LOGIN,5)==0){
		
		while(1){
			fucsia();
			
			memset(sonoio,0,sizeof(char)*(strlen(sonoio)+1));
			printf("INSERISCI NOME UTENTE: ");
			fgets(sonoio, MAX_LINE, stdin);
			res = Writeline(sockTCP, sonoio, strlen(sonoio));
			if(res <= 0) handler1();
			memset(buffer,0,sizeof(char)*(strlen(buffer)+1));
			res = Readline(sockTCP, buffer, 3);
			if(res <= 0) handler1();
			if(strcmp(buffer,"ok")==0) {
				printf("UTENTE VALIDO\n");
				break;
			}
			else printf("WARNING: UTENTE NON REGISTRATO - INSERISCI UTENTE VALIDO");
		}
		
		while(1){
			fucsia();
			printf("INSERISCI PASSWORD: ");
			memset(buffer, 0, sizeof(char)*(strlen(buffer)+1));
			fgets(buffer, MAX_LINE, stdin);
			res = Writeline(sockTCP, buffer, strlen(buffer));
			if(res<=0) handler1();
			memset(buffer,0,sizeof(char)*(strlen(buffer)+1));
			res = Readline(sockTCP, buffer, 3);
			if(res<=0) handler1();
			if(strcmp(buffer,"ok")==0) {
				printf("PASSWORD VALIDA\n");
				break;
			}
			else printf("WARNING: PASSWORD ERRATA - INSERISCI PASSWORD VALIDA");
		}	
	}
	else{
		while(1){
			fucsia();
			printf("SCEGLI UN NOME UTENTE: ");
			fgets(sonoio, MAX_LINE, stdin);
			res = Writeline(sockTCP, sonoio, strlen(sonoio));
			if(res<=0) handler1();
			res = Readline(sockTCP,buffer, 3);
			if(res<=0) handler1();
			if(strcmp(buffer,"ok")==0) {
				printf("NOME UTENTE VALIDO\n");
				break;
			}
			else printf("WARNING: NOME UTENTE GIA IN USO %s,%s\n",buffer,sonoio);
		}
		printf("SCEGLI UNA PASSWORD: ");
		fgets(buffer, MAX_LINE, stdin);
		res = Writeline(sockTCP, buffer, strlen(buffer));
		if(res<=0) handler1();
	} 
	//fine tcp
	close(sockTCP);
	
	

	remoteServAddr.sin_addr.s_addr=inet_addr(address);
	remoteServAddr.sin_family = AF_INET;
	remoteServAddr.sin_port = htons(REMOTE_SERVER_PORT);
	
	/* socket UDP */
	sd = socket(AF_INET,SOCK_DGRAM,0);
	if (sd<0){
		printf("impossibile aprire la socket \n");
		exit(1);
	}
    int sLen = sizeof(remoteServAddr);
	char dest[MAX_LINE]={0};
	
	
	while(1){ //1 while

			printf("A CHI VUOI SCRIVERE? : ");
			fgets(dest, MAX_LINE, stdin);
			

		//chiude 1 while
		while(1){ //2 while
			
			printf("Quale è il messaggio? : ");
			fgets(buffer, MAX_LINE, stdin);
			if(strncmp(buffer,BACK,8)==0) break;
			char sub[MAX_LINE] = "";
			sprintf(sub,"%s:%s:%s",dest,sonoio,buffer);
			
			char sub1[MAX_LINE] = "";
			sprintf(sub1,"%s:%s:%s",dest,sonoio,"LEGGI\n");
			rc = sendto(sd, sub1, strlen(sub1)+1, 0, (struct sockaddr *) &remoteServAddr, sizeof(remoteServAddr)); //mando messaggio
			if(rc<=0) handler1();
			memset(buffer,0,sizeof(char)*(strlen(buffer)+1));
			ret=recvfrom(sd, buffer, 2, MSG_WAITALL, (struct sockaddr *) &remoteServAddr, &sLen); //controllo se esiste la persona
			if(ret<=0) handler1();
			
			if(strcmp(buffer,"ok")!=0){
				printf("WARNING: UTENTE NON REGISTRATO - INSERISCI UTENTE VALIDO\n");
				break;
			}
	
			
			while(1){
				 memset(buffer,0,sizeof(char)*(strlen(buffer)+1));
				 ret=recvfrom(sd, buffer, MAX_LINE-1, MSG_WAITALL, (struct sockaddr *) &remoteServAddr, &sLen);
				 if(ret<=0) handler1();
				 
				 if(strncmp(buffer,END,5)==0)	break;
				
				 yellow();
				 printf("%s\n\n",buffer);
				 reset();
			} //while per ricevere tutti i mess
			
			rc = sendto(sd, sub, strlen(sub)+1, 0, (struct sockaddr *) &remoteServAddr, sizeof(remoteServAddr)); //mando messaggio
			if(rc<=0) handler1();
			yellow();
			printf("MESSAGGIO INVIATO!\n");
			reset();
			
			memset(buffer,0,sizeof(char)*(strlen(buffer)+1));
			ret=recvfrom(sd, buffer, 2, MSG_WAITALL, (struct sockaddr *) &remoteServAddr, &sLen);
			if(ret<=0) handler1();
			
			if(strcmp(buffer,"ok")==0){
				yellow();
				printf("MESSAGGIO RICEVUTO!\n");
				reset();
				
			}
			
			while(1){
				 memset(buffer,0,sizeof(char)*(strlen(buffer)+1));
				 ret=recvfrom(sd, buffer, MAX_LINE-1, MSG_WAITALL, (struct sockaddr *) &remoteServAddr, &sLen);
				 if(ret<=0) handler1();
				 
				 if(strncmp(buffer,END,5)==0)	break;
				
				 yellow();
				 printf("%s\n",buffer);
				 reset();
			} //while per ricevere tutti i mess
			
			fucsia();
			printf("1. Scrivi un messaggio da inviare\n2. INDIETRO per cambiare destinatario\n3. LEGGI per leggere\n ");
			
		
		}//chiude 3 while
	
	}//chiude primo while

    return EXIT_SUCCESS;
}


