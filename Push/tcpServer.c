#include <pthread.h>
#include <sys/socket.h>       
#include <sys/types.h>        
#include <arpa/inet.h>        
#include <unistd.h>           
#include "helper.h"           
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <assert.h>

#define MAX_LINE (1000)
#define LEGGI "LEGGI"
#define MURDERED "KILLED"
#define END "ENDED\n"
#define BACK "INDIETRO"
#define LOGIN "LOGIN"
#define REG "REGISTER"

#define LOCAL_SERVER_PORT 7007

char* uno = "uno";
char* zero = "zero";
char* due = "due";

int numnames=0; //Numero persone sul file, registrate 
int flag= 0; //Il primo thread che riceve ctrl c pulisce tutto, gli altri no
int status = 0; 
char* names[16]={0};
char* psws[16]={0};
struct chat* head = NULL; //Inizio chat
int sockTCP = 0;
int threads[20] = {0}; //Thread Socket
int sd=0;

pthread_t tudp;


pthread_t piar[30]={0}; //array di thread id 
int piarcount=0; // ogni volta che creiamo un thread mettiamo dentro piar un id, ci serve per deallocare correttamente

//-- STRDUP --//
char* temp = NULL;
char* temp1 = NULL;
char* temp2 = NULL;
char* pap = NULL;
char* d1 = NULL;
char* d2 = NULL;
char* mssg = NULL;
char* d3 = NULL;
char* d4 = NULL;


int okName(char name[], char* names[]) {
	int i=0;
	while(names[i]!=NULL){
		int ret=strncmp(name,names[i],strlen(name)-1);
		if(ret==0){
			 return i;
		 } 
		i++;
	}
	return -1;
}
int okPsw(char* psw, char* psws[],int i) {
	if(strncmp(psw,psws[i],strlen(psw)-1)==0) return 1;
	return -1;
}

void registrazione(char name[], char psw[]){
	for(int z=0; z<sizeof(names)/sizeof(names[0]);z++){
		if(names[z] == NULL){
			names[z] = name;
			psws[z] = psw;
			break;
		}
	}	
}
void gestione(){
	if(flag==0){
		yellow();
		printf("Catturato segnale\n");
		reset();
		flag=1;
		struct chat* temp5 = head;
		while(temp5!=NULL){
			struct chat* temp8 = temp5->next;
			for(int i=1; i+1<sizeof(temp5->msg)/sizeof(temp5->msg[0]);i+=2){
				if(temp5->msg[i]!=NULL){
					free(temp5->msg[i]);
				}
			}
			free(temp5->dest1);
			free(temp5->dest2);
			free(temp5);
			temp5 = temp8;
			
		}
		for(int k = numnames+1; k<16; k++){
			if(names[k]!=0){
				free(names[k]);
			}
			if(psws[k]!=0){
				free(psws[k]);
			}
		}
		free(temp5);
		free(temp1);
		free(temp2);
		free(pap);
		
		for(int k = 0; k<piarcount; k++){
			if(threads[k]!=0){
				close(threads[k]);
			}
		}
		
		for(int g=0;g<piarcount;g++){
			pthread_kill(tudp,SIGINT);
		}
		close(sockTCP);
		close(sd);
	}
	pthread_exit(NULL);
}

int Tok(int argc, char *argv[], char **nPort)
{
    int n = 1;

    while ( n < argc )
	{
		if ( !strncmp(argv[n], "-p", 2) || !strncmp(argv[n], "-P", 2) )
			*nPort = argv[++n];
		else 
			if ( !strncmp(argv[n], "-h", 2) || !strncmp(argv[n], "-H", 2) )
			{
			    printf("Sintassi Corretta :\n");
	    		printf("    server -p (porta) [-h]\n");
			    exit(EXIT_SUCCESS);
			}
		++n;
    }
	if (argc==1)
	{
	    printf("Sintassi Corretta:\n");
		printf("    server -p (porta) [-h]\n");
	    exit(EXIT_SUCCESS);
	}
    return 0;
}

void * receiveThread(void * param)
{
	char buf[MAX_LINE] ="";
	int my_sock = *((int*)param); //Descrittore accept
	int res = 0;
	char destinatario1[MAX_LINE] ="";
	char destinatario2[MAX_LINE] ="";

	
	memset(buf, 0, sizeof(char)*(strlen(buf)+1));
	
	Readline(my_sock, buf, MAX_LINE-1);
	
	if(strncmp(buf,MURDERED,6)==0){
		close(my_sock);
		pthread_exit((void *)&status);
	}
	
	if(strncmp(buf,LOGIN,5)==0) { //LOGIN
		
		while(1){ //Inserimento nome
			Readline(my_sock,destinatario1, MAX_LINE-1);
			if(strncmp(destinatario1,MURDERED,6)==0){
				close(my_sock);
				pthread_exit((void *)&status);
				
			}
			printf("dest LOGIN:%s\n",destinatario1);
			res = okName(destinatario1,names);
			if(res < 0) {
				char ack[10]="no";
				Writeline(my_sock, ack, strlen(ack));
			}
			else{
				char ack_1[10]="ok";
				Writeline(my_sock, ack_1,strlen(ack_1));
				break;
			}
		}
		while(1){ //Inserimento password
			memset(buf, 0, sizeof(char)*(strlen(buf)+1));
			Readline(my_sock, buf, MAX_LINE-1);
			if(strncmp(buf,MURDERED,6)==0){
				close(my_sock);
				pthread_exit((void *)&status);
			}
			int res1 = okPsw(buf,psws,res);
			if(res1 < 0) {
				char ack_2[10]="no";
				Writeline(my_sock, ack_2, strlen(ack_2));
			}
			else{
				char ack_3[10]="ok";
				Writeline(my_sock, ack_3,strlen(ack_3));
				break;
			}	
		}
	}
	
	else{ //REGISTRAZIONE
		while(1){
		Readline(my_sock, destinatario1, MAX_LINE-1); //Nome
		if(strncmp(destinatario1,MURDERED,6)==0){ 
			close(my_sock);
			pthread_exit((void *)&status);
		}
		printf("dest:%s\n",destinatario1);
		if(okName(destinatario1,names)!=-1){
			char ack_2[10]="no";
			Writeline(my_sock, ack_2, strlen(ack_2));
		}
		else {
			char ack[10]="ok";
			Writeline(my_sock, ack, strlen(ack));
			break;
		}
	}
		Readline(my_sock, buf, MAX_LINE-1); //Password
		if(strncmp(buf,MURDERED,6)==0){ 
			close(my_sock);
			pthread_exit((void *)&status);
		}
		pap = strdup(buf); 
		d1= strdup(destinatario1);
		registrazione(d1,pap);
		memset(buf,0,sizeof(char)*(strlen(buf)+1));
	}
	//close(my_sock);
	for(int z=0; z<sizeof(names)/sizeof(names[0]);z++){
		if(names[z] == NULL){
			break;
		}
		printf("n:%s,p:%s-\n",names[z],psws[z]);
	}	

	pthread_exit((void *)&status);
}

void * receiveThreadUDP(){
	
	char* destinatario1=NULL;
	char* destinatario2=NULL;
	
	int rc, n, cliLen, res=0;
	struct sockaddr_in cliAddr, servAddr;
  	char mess[MAX_LINE]="";
	sd=socket(AF_INET, SOCK_DGRAM, 0);
  	if(sd<0) {
    	printf("errore nell'apertura dell socket.\n");
    	exit(1);
  	}
	memset(&cliAddr, 0, sizeof(cliAddr));
  	/* bind local server port */
  	servAddr.sin_family = AF_INET;
  	servAddr.sin_addr.s_addr = INADDR_ANY;
  	servAddr.sin_port = htons(LOCAL_SERVER_PORT);
  	rc = bind (sd, (struct sockaddr *) &servAddr,sizeof(servAddr));
  	if(rc<0){
    	printf("errore nella bind %d \n",  LOCAL_SERVER_PORT);
    	exit(1);
  	}

  	printf(" in attesa di dati sulla porta UDP \n");

	cliLen = sizeof(cliAddr);
	
	while(1){ //scelta destinatiario
		
		
		
		
		while(1){ //while per l'invio continuo di messaggi che arrivano
			memset(mess, 0, sizeof(char)*(strlen(mess)+1));
			n = recvfrom(sd, mess, MAX_LINE, 0, (struct sockaddr *) &cliAddr, &cliLen);

			if (n<0) printf("impossibile ricevere dati \n");
			else printf("ricevuto dati %s\n",mess);
			
			char* token=strtok(mess,":");
			char* buff[16] ={0};
			int i=0;
			while(token!=NULL) {
				buff[i]=token;
				token = strtok(NULL,":");
				i++;
			}
			destinatario2=buff[0];
			destinatario2[strlen(destinatario2)-1]='\0';
			destinatario1=buff[1];
			destinatario1[strlen(destinatario1)-1]='\0';
			mssg=buff[2];
			mssg[strlen(mssg)-1]='\0';
			
			res=okName(destinatario2,names);
				
			if(res < 0) {
				char ack[]="no";
				rc = sendto(sd, ack, 2, MSG_CONFIRM, (struct sockaddr *) &cliAddr, cliLen);
				if(rc<0) perror("errore:");
				printf("send no fatta\n");
				break;
			}
			else{
				char ack_1[]="ok";
				rc = sendto(sd, ack_1, 2, MSG_CONFIRM, (struct sockaddr *) &cliAddr, cliLen);
				printf("send ok fatta\n");
				if(rc<0) perror("errore:");
					
			}
			
			if(head == NULL){ //Inizializzazione Linked List Chat 
				printf("dentro head null fatta\n");
				head = malloc(sizeof(struct chat));
				//pthread_mutex_init(&(head->lock),NULL); //Evitiamo che due utenti possano contemporaneamente scrivere sullo stesso buffer
				d1=strdup(destinatario1);
				d2=strdup(destinatario2);
				
				head->dest1=d1;
				head->dest2=d2;
				
				for(int j=0; j<sizeof(head->msg)/sizeof(head->msg[0]);j++){
					head->msg[j] = NULL; //Inizializzazione a NULL dell'array della chat
				}
					
				head->next=NULL;
			}
			else { //Se esiste almeno una chat, verifichiamo se ci sia una chat preesistente tra i due utenti, altrimenti viene creata
				printf("else\n");
				struct chat* t = head;
				while( !(((strncmp(t->dest1,destinatario1,strlen(t->dest1)) == 0) && (strncmp(t->dest2,destinatario2,strlen(t->dest2)) == 0)) || ((strncmp(t->dest1,destinatario2,strlen(t->dest1)) == 0) && (strncmp(t->dest2,destinatario1,strlen(t->dest2)) == 0))) && t!=NULL){
					if(t->next == NULL ){
						t->next = malloc(sizeof(struct chat));
						//pthread_mutex_init(&(t->next->lock),NULL);
						d3=strdup(destinatario1);
						d4=strdup(destinatario2);
						t->next->dest1=d3;
						t->next->dest2=d4;
						for(int j=0; j<sizeof(t->next->msg)/sizeof(t->next->msg[0]);j++){
							t->next->msg[j] = NULL;
						}	
						t->next->next=NULL;
						break;
					}
					t = t->next;
				}
			}	

			
			struct chat* te = head;
			while(te!=NULL){ //mi posiziono sulla chat giusta
				if((strcmp(te->dest1,destinatario1) == 0) && (strcmp(te->dest2,destinatario2) == 0) || ((strcmp(te->dest1,destinatario2) == 0) && (strcmp(te->dest2,destinatario1) == 0) )){
					break;
				}
				te=te->next;
			}
			
			//controllo che messaggio ho ricevuto
			if(strncmp(mssg," LEGGI",5)==0 || strncmp(mssg,LEGGI,5)==0){ //Per leggere messaggi non letti
				printf("LEGGO!!!\n");
				for(int i=0; i+1<sizeof(te->msg)/sizeof(te->msg[0]);i+=2){ //Invio dei messaggi letti dall'altro interlocutore
						if(te->msg[i] == NULL) break;
						printf("mitt:%s mess:%s\n",destinatario1,te->msg[i+1]);
						if((strcmp(te->msg[i],due)==0) && strncmp(te->msg[i+1],destinatario1,strlen(destinatario1)-1)==0){
							printf("mss:%s\n",te->msg[i+1]);
							char doppia[MAX_LINE] = "";
							sprintf(doppia,"%s ha letto %s",destinatario2, te->msg[i+1]);
							rc = sendto(sd, doppia , strlen(doppia), 0, (struct sockaddr *) &cliAddr, sizeof(cliAddr));
							if(rc<0) printf("errore");
							te->msg[i] = zero;
						}
				}
				
				for(int i=0; i+1<sizeof(te->msg)/sizeof(te->msg[0]);i+=2){ //Per leggere messaggi non letti
						if(te->msg[i] == NULL) break;
						printf("mitt:%s mess:%s\n",destinatario1,te->msg[i+1]);
						if((strcmp(te->msg[i],uno)==0) && strncmp(te->msg[i+1],destinatario1,strlen(destinatario1)-1)!=0){
							printf("mss:%s\n",te->msg[i+1]);
							rc = sendto(sd, te->msg[i+1] , strlen(te->msg[i+1]), 0, (struct sockaddr *) &cliAddr, sizeof(cliAddr));
							if(rc<0) printf("errore");
							te->msg[i] = due;
						}
				}

				rc = sendto(sd, END,6, 0, (struct sockaddr *) &cliAddr, sizeof(cliAddr));
				memset(mess, 0, sizeof(char)*(strlen(mess)+1));
			}
			
			else { //se il messaggio non è leggi lo inseriamo con flag 1
				yellow();
				printf("Server: Inserisco nella chat il messaggio\n");
				char buftemp[50]="";
				strcpy(buftemp,mssg);
				for(int i=0; i+1<sizeof(te->msg)/sizeof(te->msg[0]);i+=2){
					if(te->msg[i] == NULL) {
						printf("dest:%s mitt:%s\n",te->dest1,te->dest2);
						te->msg[i] = uno;
						char t1[MAX_LINE]="";
						char sub[MAX_LINE] = "";
						strncpy(sub,destinatario1,strlen(destinatario1));
						sprintf(t1,"%s : %s",sub,mssg);
						printf("%s",t1);
						mssg=strdup(t1);
						te->msg[i+1] = mssg;
						break;
					}	
				}
				rc = sendto(sd, mssg,strlen(mssg), 0, (struct sockaddr *) &cliAddr, sizeof(cliAddr));
				if(rc<0) printf("errore send");
				rc = sendto(sd, END,6, 0, (struct sockaddr *) &cliAddr, sizeof(cliAddr));
				if(rc<0) printf("errore send");
				}
			
			
		}//chiude while messaggi
			
			
			
		
	}//chiude cambio destinatario 1 while
	
		
}




int main(int argc, char *argv[])
{
                  
    pthread_t tid;
    short int port;                 
    struct    sockaddr_in servaddr; 
    struct	  sockaddr_in their_addr;
	char      buffer[MAX_LINE];      
	char      buffer1[MAX_LINE];  
	char     *endptr;                
	int 	  sin_size;    
	int 	  proc_id;
	void     *status;
	int       client_id;   
	int k=0;
	
	signal(SIGINT,gestione);
	signal(SIGQUIT,gestione);
	
	int res;
    pthread_attr_t attr;
    

    res = pthread_attr_init(&attr);
    if (res != 0) {
        perror("Attribute init failed");
        exit(EXIT_FAILURE);
    }
    res = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    if (res != 0) {
        perror("Setting detached state failed");
        exit(EXIT_FAILURE);
    }


	
	Tok(argc, argv, &endptr);
	
	
	FILE* f = fopen("miofile.txt", "r");
	assert(f!= NULL);
	char str[512] = "";
	int r = fread(str,sizeof(char),512,f);
	fclose(f);
	
	char* token=strtok(str,", ;");
	char* buff[16] ={0};
	int i=0;
	while(token!=NULL) {
		buff[i]=token;
		token = strtok(NULL,", \n ;");
		i++;
	}
	for(k=0,numnames=0; k+1<sizeof(buff)/sizeof(buff[0]) && numnames<sizeof(buff)/sizeof(buff[0]);numnames++,k+=2){
		if(buff[k]!=NULL){
				names[numnames]=buff[k];
				psws[numnames]=buff[k+1];
		}
	}
	
	port = strtol(endptr, &endptr, 0); //porta in long int
	
	if (*endptr)
	{
	    fprintf(stderr, "Server: Porta non riconosciuta.\n");
	    exit(EXIT_FAILURE);
	}
	printf("Server in ascolto sulla porta %d.\n",port);
	

	
    if ((sockTCP = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		fprintf(stderr, "Server: Errore nella creazione della socket.\n");
		exit(EXIT_FAILURE);
    }

    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port        = htons(port);


 
    if (bind(sockTCP, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0)
	{
		fprintf(stderr, "Server: Errore durante la bind.\n");
		exit(EXIT_FAILURE);
    }

    if (listen(sockTCP, 16) < 0)
	{
		fprintf(stderr, "Server: Errore durante la listen.\n");
		exit(EXIT_FAILURE);
    }
    
    
    
    int id = pthread_create(&tudp, &attr, receiveThreadUDP, NULL);
    
	
    while (1)
	{
		sin_size = sizeof(struct sockaddr_in);
		if ((client_id = accept(sockTCP, (struct sockaddr *)&their_addr, &sin_size)) < 0)
		{
		    fprintf(stderr, "Server: \n");
	    	exit(EXIT_FAILURE);
		}
		printf("Server: Si è connesso il client con IP %s\n", inet_ntoa(their_addr.sin_addr));
		proc_id = pthread_create(&tid, &attr, receiveThread, (void*)&client_id);
		threads[piarcount] = client_id;
		piar[piarcount] = tid;
		piarcount+=1;
		

		if (proc_id)
		{	
			printf("Server: Impossibile creare il thread. Errore: %d\n", proc_id);
      		exit(EXIT_FAILURE);
      	}		
    }
	
	
	
}


